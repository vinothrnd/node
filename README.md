# Node MongoDB
Simple application using NodeJs and MongoDB

## Prerequisite
1. NodeJS
2. MongoDB

## Install depdencies

``` sh
npm install
```

## Database config

To replace the database host and port values under `config.js`

## Start application

``` sh
npm run start
```

Server running http://localhost:8085/
