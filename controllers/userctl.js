'use strict';
const Q = require('q');
const mongoose = require('mongoose');
require('../models/user');
var user = mongoose.model('userModel');
mongoose.Promise = Q.Promise;

module.exports.userdata = (req, res, next) => {
    console.log(req.body);
    user.create(req.body, function (err, response) {
        if (err) {
            return next(err);
        }
        if (response) {
            return res.status(200).send('created..');
        }
    });
}