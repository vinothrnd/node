'use strict';
const mongoose = require('mongoose');
var Q = require('q');
var db = mongoose.connection;
const mongoduri = require('mongodb-uri');
const config = require('./config');
mongoose.Promise = Q.Promise;
var testdb = 'mongodb://' + config.database.host + ':' + config.database.port + '/test';
mongoose.connect(mongoduri.formatMongoose(testdb), { useCreateIndex: true });

var testconn = function () {
    var defered = Q.defer();
    var db = mongoose.connection;
    db.once('open', () => {
        console.log('db connetion establised.');
        defered.resolve();
    });

    db.on('discon', function () {
        console.log('database discon..');
    });

    db.on('error', function errCon(err) {
        console.log('error connec....' + err);
        process.exit(0);
    });

    return defered.promise;
}

process.on('SIGINT', () => {
    Q.allSettled([mongoose.connection.close()]).then(() => { process.exit(0); });
});

module.exports.testconn = testconn;