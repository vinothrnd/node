const mongoose = require('mongoose');
var userModel = new mongoose.Schema({
    Name: {
        type: String,
        trim: true,
        required: [true, 'First name required']
    },
    Email: {
        type: String,
        trim: true,
        sparse: true,
    },
    DOB: {
        type: Date,
        trim: true,
    },
    Mobile: {
        type: Number,
        trim: true,
        unique: true
    }

}, { versionKey: false, collection: 'user' });

mongoose.model('userModel', userModel);
